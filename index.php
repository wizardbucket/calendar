<?php

$currentYear = date('Y');
$currentMonth = date('m');

    if (isset($_POST['acceptDate']))   {
        $year = $_POST['yearValue'];
        $month = $_POST['monthValue'];

        $currentYear = $year;
        $currentMonth = $month;

        buildTable($year, $month);
    }

    else if (isset($_POST['next']))   {

          $year = $_POST['yearValue'];

          if($_POST['monthValue']>=12){
            $month = 12;
          }
          else{
            $month = $_POST['monthValue']+1;
          }

          $currentYear = $year;
          $currentMonth = $month;

          buildTable($year, $month);
        }

    else if (isset($_POST['prev']))   {

          $year = $_POST['yearValue'];

          if($_POST['monthValue']<=1){
            $month = 1;
          }
          else{
            $month = $_POST['monthValue']-1;
          }

          $currentYear = $year;
          $currentMonth = $month;

          buildTable($year, $month);
        }
    else{
      buildTable($currentYear, $currentMonth);
    }

echo "<form action='index.php' method='POST'>";
echo "<p>Введите год: <input name='yearValue' type='text' value='$currentYear'></p>";
echo "<p>Введите месяц: <input name='monthValue' type='text' value='$currentMonth'></p>";
echo "<p><input name='prev' type='submit' value='Предыдущий'> <input name='next' type='submit' value='Следующий'></p>";
echo "<input name='acceptDate' type='submit' value='Подтвердить дату' />";
echo "</form>";

function buildTable($year, $month){

    $days_in_month = date('t');
    $day_counter = 1;
    $weekdays = ["Пн","Вт","Ср","Чт","Пт","Сб","Вс"];

    $num = 0;

    for($i = 0; $i < 7; $i++){
      $dayofweek = date('w', mktime(0, 0, 0, $month, $day_counter, $year));
      $dayofweek = $dayofweek - 1;

      if($dayofweek == -1){
        $dayofweek = 6;
      }

      if($dayofweek == $i){
        $week[$num][$i] = $day_counter;
        $day_counter++;
      }

      else{
        $week[$num][$i] = "";
      }

  }

  while(true){
    $num++;

    for($i = 0; $i < 7; $i++){
      $week[$num][$i] = $day_counter;
      $day_counter++;
      if($day_counter > $days_in_month){
        break;
        }
    }

    if($day_counter > $days_in_month){
        break;
      }
  }

  $monthText = date('F',mktime(0,0,0,$month));
  echo ($monthText);
  echo "<table border=1>";

  echo "<tr>";

  foreach ($weekdays as $day) {
      echo "<td>";
      echo $day;
      echo "</td>";
  }

  echo "</tr>";

  for($i = 0; $i < count($week); $i++){
    echo "<tr>";

    for($j = 0; $j < 7; $j++){

      if(!empty($week[$i][$j])){

        if($j == 5 || $j == 6){

             if($week[$i][$j] == date('d') && $month == date('m') && $year == date('Y')){
                echo "<td><font color=blue>".$week[$i][$j]."</font></td>";
             }
             else{
                echo "<td><font color=red>".$week[$i][$j]."</font></td>";
             }

           }
           else{

             if($week[$i][$j] == date('d') && $month == date('m') && $year == date('Y')){
                echo "<td><font color=blue>".$week[$i][$j]."</font></td>";
             }
             else{
                echo "<td>".$week[$i][$j]."</td>";
             }
           }
      }

      else echo "<td>&nbsp;</td>";

    }

    echo "</tr>";

  }

  echo "</table>";
}

?>
